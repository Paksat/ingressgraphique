// Faire commencer le tableau par reconnaitre l'équipe et faire -1 pour l'indice et commencer par le nom ?
// Faire une hashMap
// LifeTime AP c'est pour ceux qui rerooll (le phoenix)
// Utiliser JSON pour parser (tp4)
// lien zingchart https://www.zingchart.com/gallery/transparent-spline-area-chart

let mesValeurs = `Time Span Agent Name Agent Faction Date (yyyy-mm-dd) Time (hh:mm:ss) Level Lifetime AP Current AP Unique Portals Visited XM Collected Distance Walked Resonators Deployed Links Created Control Fields Created Mind Units Captured Longest Link Ever Created Largest Control Field XM Recharged Portals Captured Unique Portals Captured Mods Deployed Resonators Destroyed Portals Neutralized Enemy Links Destroyed Enemy Fields Destroyed Max Time Portal Held Max Time Link Maintained Max Link Length x Days Max Time Field Held Largest Field MUs x Days Unique Missions Completed Hacks Glyph Hack Points Longest Hacking Streak
TOUS TEMPS Paksat Enlightened 2020-03-10 23:45:25 6 539308 539308 332 226060 34 915 4 2 33 0 20 2839 702 176 6 66 37 9 3 4 1 0 1 30 30 535 318 6 `;

let mesValeurs2 = `Time Span Agent Name Agent Faction Date (yyyy-mm-dd) Time (hh:mm:ss) Level Lifetime AP Current AP Unique Portals Visited XM Collected Distance Walked Resonators Deployed Links Created Control Fields Created Mind Units Captured Longest Link Ever Created Largest Control Field XM Recharged Portals Captured Unique Portals Captured Mods Deployed Resonators Destroyed Portals Neutralized Enemy Links Destroyed Enemy Fields Destroyed Max Time Portal Held Max Time Link Maintained Max Link Length x Days Max Time Field Held Largest Field MUs x Days Unique Missions Completed Hacks Glyph Hack Points Longest Hacking Streak
TOUS TEMPS Paksat Enlightened 2020-03-11 22:54:40 6 540175 540175 332 226960 34 915 4 2 33 0 20 2839 702 176 6 66 37 9 3 5 1 0 1 30 30 540 325 6`;

let mesValeurs3 = `Time Span Agent Name Agent Faction Date (yyyy-mm-dd) Time (hh:mm:ss) Level Lifetime AP Current AP Unique Portals Visited XM Collected Distance Walked Resonators Deployed Links Created Control Fields Created Mind Units Captured Longest Link Ever Created Largest Control Field XM Recharged Portals Captured Unique Portals Captured Mods Deployed Resonators Destroyed Portals Neutralized Enemy Links Destroyed Enemy Fields Destroyed Max Time Portal Held Max Time Link Maintained Max Link Length x Days Max Time Field Held Largest Field MUs x Days Unique Missions Completed Hacks Glyph Hack Points Longest Hacking Streak
TOUS TEMPS Paksat Enlightened 2020-03-12 22:54:40 6 540175 540175 332 226960 34 915 4 2 33 0 20 2839 702 176 6 66 37 9 3 5 1 0 1 30 30 540 325 6`;

let tab = mesValeurs.split(' ');
let tab2 = mesValeurs2.split(' ');
let tab3 = mesValeurs3.split(' ');

document.querySelector(
	'#infoJoueur'
).innerHTML += `${tab[91]} tu fais partit de l'équipe des : ${tab[92]}. <br> Nous somme le ${tab[93]} à ${tab[94]} et tu es niveau ${tab[95]}`;

document.querySelector(
	'#infoJoueur'
).innerHTML += `<br><br><br>${tab2[91]} tu fais partit de l'équipe des : ${tab2[92]}. <br> Nous somme le ${tab2[93]} à ${tab2[94]} et tu es niveau ${tab2[95]}`;

// test
let tabDate = [tab[93], tab2[93], tab3[93]];
console.log(tabDate);

let tabDateConvert;
let myDate = 0;

for (let i = 0; i < tabDate.length; i++) {
	myDate = tabDate[i].split('-');
	//Base : 26-02-2012
	// Moo : 2020-03-11
	var newDate = myDate[1] + '/' + myDate[2] + '/' + myDate[0];

	console.log(new Date(newDate).getTime()); //will alert 1330210800000
}

console.log(tabDateConvert);
/*
var myDate="26-02-2012";
myDate=myDate.split("-");
var newDate=myDate[1]+"/"+myDate[0]+"/"+myDate[2];
alert(new Date(newDate).getTime()); //will alert 1330210800000
*/

let chartConfig = {
	type: 'area',
	stacked: true,
	title: {
		text: 'Je suis le test du canard',
		fontColor: '#4caee7',
	},
	subtitle: {
		text: 'Pour le jsp',
		fontColor: '#cb8670',
	},
	/*
		ANIMATION
	plot: {
		activeArea: true,
		animation: {
			delay: 50,
			effect: 'ANIMATION_SLIDE_LEFT',
			method: 'ANIMATION_REGULAR_EASE_OUT',
			sequence: 'ANIMATION_NO_SEQUENCE',
			speed: 1500,
		},
		hoverMarker: {
			size: '8px',
		},
	},*/
	plotarea: {
		backgroundColor: '#fff',
	},
	scaleX: {
		lineColor: '#333',
		/*maxItems: 6,
		minValue: 1274313600000,
		step: 600000,
		*/
		/*minValue: 1583967280000,
		step: 60000000,
		*/
		minValue: tabDateConvert,
		step: 10000000,
		tick: {
			lineColor: '#333',
		},
		transform: {
			type: 'date',
			all: '%D, %d %M <br>%h:%i %A ' /*,*/,
		},
	},
	scaleY: {
		item: {
			padding: '4px',
		},
		lineColor: '#333',
		tick: {
			lineColor: '#333',
		},
	},
	series: [
		{
			values: [11, 36, 7, 44, 11, 28, 42, 26, 13, 32, 12, 24, 16, 11, 43, 39],
			backgroundColor: '#cb8670',
			lineColor: '#cb8670',
			/*
			Pour les points
			marker: {
				backgroundColor: '#cb8670',
				borderColor: '#fff',
				borderWidth: '2px',
			},*/
		},
	],
};

zingchart.render({
	id: 'myChart',
	data: chartConfig,
	height: '100%',
	width: '100%',
});
